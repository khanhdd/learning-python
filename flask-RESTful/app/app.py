from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from security import authenticate, identity
from flask_jwt import JWT, jwt_required

app = Flask(__name__)
app.secret_key = 'khanhdd'
api = Api(app)

jwt = JWT(app, authenticate, identity)  # /auth
items = []

# Defining our resource

class Item(Resource):
    # http method this resource accept
    # def get(self, name):
    #     for item in items:
    #         if (item['name'] == name):
    #             return item, 200
    #     return {'item': None}, 404
    def req_parser(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'price',
            type=float,
            required=True,
            help='This field cannot be left blank!'
        )
        data = parser.parse_args()
        return data

    @jwt_required()
    def get(self, name):
        item = next(filter(lambda x: x['name'] == name, items), None)
        return {'item': item}, 200 if item else 404

    def post(self, name):
        if next(filter(lambda x: x['name'] == name, items), None):
            return {'item': None}, 400
        data = self.req_parser()
        new_item = {'name': name, 'price': data['price']}
        items.append(new_item)
        return new_item, 201

    def delete(self, name):
        global items
        items = list(filter(lambda x: x['name'] != name, items))
        return {'message': 'item deleted'}

    def put(self, name):
        data = self.req_parser()
        item = (next(filter(lambda x: x['name'] == name, items), None))
        if item:
            # item['price'] = data['price']
            item.update(data)
        else:
            item = {'name': name, 'price': data['price']}
            items.append(item)
        return item

class ItemList(Resource):
    def get(self):
        return {'items': items}

# Add resource to an api
api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')
# app.run(port=5000)
# app.run(host='0.0.0.0',port=5000)
if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False)
