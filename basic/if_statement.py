# exercise 1
def who_do_you_know():
    user_input = input("Who do you know ? \n")
    people_list = [person.strip() for person in user_input.split(",")]
    return people_list

# exercise 2
def ask_user():
    person = input("Name someone you know ? \n")
    if person.strip() in who_do_you_know():
        print("You know {}!".format(person.strip()))
    else:
         print("You don't know {}!".format(person.strip()))
    
ask_user()
    