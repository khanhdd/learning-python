my_list = [0, 1, 2, 3, 4, 5]
an_equal_list = [x for x in range(5)]

even_list = [n for n in range(10) if n % 2 == 0]

print(even_list)