# iterables are strings, lists, sets, tuples,...
# my_string = "hello"
# for char in my_string:
#     print(char)    

# user_wants_number = True

# while user_wants_number == True:
#     print(10)
#     user_input = input("Should we continue ? (y/n)")
#     if user_input == "n":
#         user_wants_number = False
#     else:
#         user_wants_number = True

# know_people = ["Anna", "Marry", "John", "Wick"]
# person = input("Enter the person you know: ")

# if person in know_people:
#     print("You know {}!".format(person))
# else:
#     print("You don't know {}!".format(person))

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

# Modify the method below to make sure only even numbers are returned.
def even_numbers():
    evens = []
    for number in numbers:
        if (number % 2) == 0:
            evens.append(number)
    return evens



# Modify the below method so that "Quit" is returned if the choice parameter is "q".
# Don't remove the existing code
def user_menu(choice):
    if choice == "a":
        return "Add"
    elif choice == "q":
        return "Quit"

print(even_numbers())
print(user_menu("q"))



