lists = []
# list is mutable, most flexible of 3
tuples = (76, 89, 54, 90, 96)
# tuple is immutable object,
# cannot increase size of tuple, cannot change values in tuple
tuples = tuples + (100,)
# but we can calculate new tuple
sets = {76, 89, 54, 90, 96}
your_lottery_numbers = {1, 2, 5, 6, 8}
winning_numbers = {1, 8, 9, 10, 12}
print(your_lottery_numbers.difference(winning_numbers))
print(your_lottery_numbers.union(winning_numbers))
print(your_lottery_numbers.intersection(winning_numbers))
# set is unique & unordered, cannot change values in sets
