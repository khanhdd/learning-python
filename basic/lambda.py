my_list = [13, 55, 77,  484]


def not_thirteen(x):
    return x != 13


print(list(filter(lambda x: x != 13, my_list)))
print([x for x in my_list if x!= 13])

def f(x):
    return x * 3


print(f(5))
print((lambda x: x * 3)(5))

