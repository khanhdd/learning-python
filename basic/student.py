class Student:
    def __init__(self, name, school):
        self.name = name
        self.school = school
        self.mark = []

    def average_mark(self):
        # total = sum(self.mark)
        # count = len(self.mark)
        return sum(self.mark) / len(self.mark)


anna = Student("Anna", "MIT")
anna.mark.append(56)
anna.mark.append(4)
print(anna.average_mark())


class Store:
    def __init__(self, name):
        self.name = name
        self.items = []
        # You'll need 'name' as an argument to this method.
        # Then, initialise 'self.name' to be the argument, and 'self.items' to be an empty list.

    def add_item(self, name, price):
        self.items.append({
            'name': name,
            'price': price
        })
        # Create a dictionary with keys name and price, and append that to self.items.

    def stock_price(self):
        return sum(item['price'] for item in self.items)
        # Add together all item prices in self.items and return the total.
