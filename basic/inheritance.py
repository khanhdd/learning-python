class Student:
    def __init__(self, name, school):
        self.name = name
        self.school = school
        self.marks = []

    def average(self):
        return sum(self.marks) / len(self.marks)

    @classmethod
    # *args : pass multiple arguments - return a tuple
    # **kwargs (keyword arguments) : return a set of argument with default value
    def friend(cls, origin, friend_name, *args, **kwargs):
        return cls(friend_name, origin.school, *args, **kwargs)


class WorkingStudent(Student):
    def __init__(self, name, school, salary, job_title):
        super().__init__(name, school)
        self.salary = salary
        self.job_title = job_title


anna = WorkingStudent("Anna", "RMIT", 20, "Dev")
print(anna.job_title)
friend = WorkingStudent.friend(anna, "Greg", 17.50, job_title="Dev")
print(friend.job_title)
