# Virtual Enviroment
Setting up a fresh enviroment for develop new Python's project

Step 1: 
```powershell
# view current python libraries
pip freeze
```

Step 2:
```powershell
# install venv
pip install virtualenv
```

Step 3:
```powershell
# create venv folder in current dir for python's venv
virtualenv venv --python=python<version>
# another way
virtualenv venv -p <PATH_TO_PYTHON.EXE>
```

Step 4:
```powershell
# active venv
# mac/linux
source venv/bin/active
# windows
.\venv\Scripts\activate.bat
```

Optional:
```powershell
# active venv in powershell
.\venv\Scripts\activate.bat
# Change system's execution policy to RemoreSigned or AllSigned
Set-ExecutionPolicy RemoteSigned
```