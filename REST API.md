# What is REST API
* Way of thinking about how server responds to your requests
* It doesn't respond with just data 
* It respond with resources (object oriented programming)
* Think of server as having resources

# REST API is stateless
* One request cannot depend on any other requests
* Server only know about the current request, and not any previous request

# REST Verb
GET: Read 
PUT: Update Resources
DELETE: Remove Resources
POST: Creating Resources

# Advantage vs The other
* Don't have to install anything on client side. Only make a simple HTTP request to target API service
* Statelessness
* Caching 
* Ease of coding
* Limited resources and bandwidth (XML vs JSON)
* Security

